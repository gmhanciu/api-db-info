<?php

//error_reporting(0);

require __DIR__ . '/../vendor/autoload.php';

use Pecee\SimpleRouter\SimpleRouter;

SimpleRouter::setDefaultNamespace('\Api');

require_once __DIR__ . '/../api/routes.php';

SimpleRouter::start();