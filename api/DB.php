<?php

namespace Api;

class DB
{
    private $DBConfig;
    private $PDO;

    public function __construct()
    {
        $this->loadConfig();
        $this->start();
    }

    /**
     * @return mixed
     */
    public function getDBConfig()
    {
        return $this->DBConfig;
    }

    /**
     * @param mixed $DBConfig
     */
    public function setDBConfig($DBConfig)
    {
        $this->DBConfig = $DBConfig;
    }

    /**
     * @return mixed
     */
    public function getPDO()
    {
        return $this->PDO;
    }

    /**
     * @param mixed $PDO
     */
    public function setPDO($PDO)
    {
        $this->PDO = $PDO;
    }

    private function loadConfig()
    {
        $DBConfigPath = __DIR__ . "/config.php";
        $config = require $DBConfigPath;
        $this->setDBConfig($config);
    }

    private function start()
    {
        $dsn = $this->DBConfig['DB_DATABASE_DRIVER']
            . ':host=' . $this->DBConfig['DB_HOST']
            . ';dbname=' . $this->DBConfig['DB_DATABASE']
            . ';port=' . $this->DBConfig['DB_PORT']
            . ';charset=' . $this->DBConfig['DB_CHARSET'];

        $user = $this->DBConfig['DB_USERNAME'];
        $pass = $this->DBConfig['DB_PASSWORD'];

        $options = $this->DBConfig['OPTIONS'];

        try
        {
            $this->setPDO(new \PDO($dsn, $user, $pass, $options));
        }
        catch (\PDOException $e)
        {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    protected function close()
    {
        $this->setPDO(null);
    }

}