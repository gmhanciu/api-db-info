<?php

use Pecee\Http\Request;
use Pecee\SimpleRouter\SimpleRouter;


SimpleRouter::get('/', function () {
    header('Location: ' . SimpleRouter::getUrl('tables'));
    exit();
});
SimpleRouter::get('/tables', 'Query@getDBTables')->name('tables');
SimpleRouter::get('/columns', 'Query@getDBColumnsByTables')->name('columns');