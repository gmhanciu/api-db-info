<?php

namespace Api;

use PDO;
use RecursiveArrayIterator;

class Query extends DB
{

//    private $tables;

    public function __construct()
    {
//        Helpers::cors();
        header('Access-Control-Expose-Headers: Access-Control-Allow-Origin');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: X-Requested-With, XMLHttpRequest, Origin, Content-Type, X-Auth-Token , Authorization');
        header('Content-Type: application/json');
        parent::__construct();
    }

//    /**
//     * @return mixed
//     */
//    public function getTables()
//    {
//        return $this->tables;
//    }
//
//    /**
//     * @param mixed $tables
//     */
//    public function setTables($tables)
//    {
//        $this->tables = $tables;
//    }

    public function getDBTables()
    {
        $query = "SELECT table_name
                FROM information_schema.tables
                WHERE table_schema = (:DB_DATABASE)
                ORDER BY table_name";

        $query = $this->getPDO()->prepare($query);

        $query->bindParam(':DB_DATABASE', $this->getDBConfig()['DB_DATABASE']);

        $query->execute();

        $query->setFetchMode(PDO::FETCH_ASSOC);

        $results = [];

        foreach( (new RecursiveArrayIterator($query->fetchAll())) as $table_name )
        {
//            $results[] = reset($table_name);
            $results[] = $table_name['table_name'];
        }

//        $this->setTables($results);

        //        return $results;
        return json_encode($results);

//
//        return $this->getTables();
    }

    public function getDBColumnsByTables()
    {
        $query = "SELECT table_name, column_name
                FROM information_schema.columns
                WHERE table_schema = (:DB_DATABASE)
                ORDER BY table_name, ordinal_position";

        $query = $this->getPDO()->prepare($query);

        $query->bindParam(':DB_DATABASE', $this->getDBConfig()['DB_DATABASE']);

        $query->execute();

        $query->setFetchMode(PDO::FETCH_ASSOC);

//        // we can already get all the tables from the previous query
//        $results = $this->getTables();
//
//        //flip the array because it is like $key => $table_name
//        //we want the $table_name to be the key
//        $results = array_flip($results);
//
//        //fill all the keys with empty arrays as we're aiming for structure like
//        //$table_name => [$columns]
//        $results = array_fill_keys(array_keys($results), []);

        $results = [];

        foreach( ( new RecursiveArrayIterator($query->fetchAll()) ) as $columnWithTableName )
        {
            if (!isset($results[$columnWithTableName['table_name']]))
            {
                $results[$columnWithTableName['table_name']] = [];
            }

            $results[$columnWithTableName['table_name']][] = $columnWithTableName['column_name'];
            //reset $columnWithTableName = table_name
            //end $columnWithTableName = column_name
//            $results[reset($columnWithTableName)][] = end($columnWithTableName);
        }

//        return $results;
        return json_encode($results);

//        $this->setColumnsByTables($results);
//
//        return $this->getColumnsByTables();
    }

}